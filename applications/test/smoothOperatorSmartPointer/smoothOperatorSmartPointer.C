/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2014 sourceflux 
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Application
    smoothOperatorSmartPointer

Description
    Test application for the C++ for OpenFOAM training.

Authors 
    Tomislav Maric tomislav@sourceflux.de 

\*---------------------------------------------------------------------------*/

#include "messageStream.H"
#include "word.H"
#include "refCount.H"
#include "tmp.H"
#include "autoPtr.H"

using namespace Foam;

class smoothOperator
: 
    public refCount 
{
    word operation_;

    public: 

        smoothOperator() : operation_() {}

        smoothOperator(const word operation)
            : 
                operation_(operation)
        {}

        virtual ~smoothOperator() {}; 

        virtual void operate() const
        {
            Info << "Smooth operator..." << endl;
        }

        word operation() const
        {
            return operation_; 
        }

}; 

class smootherOperator 
: 
    public smoothOperator
{
    public: 
        virtual void operate() const
        {
            Info << "Smoother operator..." << endl;
        }

};

class smoothestOperator
: 
    public smoothOperator
{
    public: 
        virtual void operate() const
        {
            Info << "Smoothest operator..." << endl;
        }
};

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
// Main program:

int main(int argc, char *argv[])
{
    // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
    
    tmp<smoothOperator> s (new smootherOperator()); 

    s->operate(); 

    Info<< "\nEnd\n" << endl;
    return 0;
}


// ************************************************************************* //
