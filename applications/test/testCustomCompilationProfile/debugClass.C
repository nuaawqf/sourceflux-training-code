/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2014 held by original authors 
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "debugClass.H"

namespace Foam {

    // Comment out when only a part of the macro is uncommented below.
    defineTypeNameAndDebug(debugClass, 0); 

    // Partial macro. 
    //const ::Foam::word debugClass::typeName(debugClass::typeName_());
    //int debugClass::debug(::Foam::debug::debugSwitch(debugClass::typeName_(), 0));

// * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * * //

void debugClass::calculateSomething()
{
    if(debug)
    {
        Info << "debugClass::calculateSomething | debug/test code." << endl;
    }

    Info << "debugClass::calculateSomething | regular code. " << endl; 

}

// ************************************************************************* //

} // End namespace Foam

// ************************************************************************* //
