/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2015 sourceflux 
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Application
    cavityPostProcess 

Description
    Application that computes the stream function and reports validation data
    for the lid driven cavity case.

Literature

    BATCHELOR, George Keith. An introduction to fluid dynamics. Cambridge
    university press, 2000. 

    BRUNEAU, Charles-Henri y SAAD, Mazen. "The 2D lid-driven cavity problem
    revisited". Computers &amp; Fluids. 2006, vol 35, n�m. 3, p. 326--348.

    MATYKA, Maciej. "Solution to two-dimensional Incompressible Navier-Stokes
    Equations with SIMPLE, SIMPLER and Vorticity-Stream Function Approaches.
    Driven-Lid Cavity Problem: Solution and Visualization". arXiv preprint
    physics/0407002. 2004, 

Authors
    Tomislav Maric tomislav@sourceflux.de

\*---------------------------------------------------------------------------*/

#include "fvCFD.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
// Main program:

int main(int argc, char *argv[])
{
    #include "setRootCase.H"
    #include "createTime.H"
    Foam::instantList timeDirs = Foam::timeSelector::select0(runTime, args);
    #include "createMesh.H"

    dimensionedVector k("k", dimless, vector(0,0,1));

    Info<< "Reading field psi\n" << endl;
    volScalarField psi
    (
        IOobject
        (
            "psi",
            runTime.timeName(),
            mesh,
            IOobject::MUST_READ,
            IOobject::AUTO_WRITE
        ),
        mesh
    );

    forAll(timeDirs, timeI)
    {
        runTime.setTime(timeDirs[timeI], timeI);

        Info<< "Time = " << runTime.timeName() << Foam::endl;

        Info<< "Reading field U\n" << endl;

        volVectorField U
        (
            IOobject
            (
                "U",
                runTime.timeName(),
                mesh,
                IOobject::MUST_READ,
                IOobject::AUTO_WRITE
            ),
            mesh
        );

        Info<< "Creating field curl\n" << endl;
        volVectorField curl 
        (
            IOobject
            (
                "curl",
                runTime.timeName(),
                mesh,
                IOobject::NO_READ,
                IOobject::AUTO_WRITE
            ),
            fvc::curl(U)
        );

        fvScalarMatrix psiEqn 
        (
            fvm::laplacian(psi) == (curl & k) 
        );

        solve(psiEqn);

        scalar convergenceError = 0; 
        
        if (timeI > 0)
        {
            // Write the fields. 
            psi.write();
            curl.write(); 

            // Revert the time to the previous time directory.
            runTime.setTime(timeDirs[timeI-1], timeI-1);
            volVectorField Uold
            (
                IOobject
                (
                    "U",
                    runTime.timeName(),
                    mesh,
                    IOobject::MUST_READ,
                    IOobject::AUTO_WRITE
                ),
                mesh
            );
            convergenceError = average(
                mag((U.internalField() - Uold.internalField())) / 
                mag(U.internalField())
            );
            // Revert the simulation time to the current time directory. 
            runTime.setTime(timeDirs[timeI], timeI);
        }

        curl.write(); 

        Info << "U average relative difference: " << convergenceError << endl; 
        Info << "min(psi) : " << min(psi.internalField()) << endl;
        Info << "max(psi) : " << max(psi.internalField()) << endl;
    }

    // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

    Info<< "\nEnd\n" << endl;
    return 0;
}


// ************************************************************************* //
